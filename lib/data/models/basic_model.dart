/// Базовая модель данных сервера
import '../../domain/data_sources.dart';
import '../data_sources.dart';


/// Класс коннектор к генератору запросов
class TypeBasicModel implements Type0 {
  static BasicModel fromJson(Map<String, dynamic> json) => BasicModel.fromJson(json);
}

/// Класс модели данных сервера
class BasicModel extends Type0 {
  BasicModel({
      required bool success,
      required String error,}){
    _success = success;
    _error = error;
}

  BasicModel.fromJson(dynamic json) {
    _success = json['success'];
    _error = json['error'];
  }

  bool _success = false;
  String _error = "";

  bool get success => _success;
  String get error => _error;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = _success;
    map['error'] = _error;
    return map;
  }

}