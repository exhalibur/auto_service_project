import '../data_sources.dart';

/// success : true
/// error : ""
/// result : [{"id":1,"name":"NordService","address":"г. Челябинск, ул Пушкина, 17","photo":"https://domen.ru/someimage.jpg","car_brands":[{"id":1,"name":"AUDI"}],"work_types":[{"id":1,"name":"Работа с двигателем"}],"latitude":56.77181,"longitude":61.89191}]

/// Класс коннектор к генератору запросов
class TypeCarServiceModel implements Type0 {
  static CarServiceModel fromJson(Map<String, dynamic> json) => CarServiceModel.fromJson(json);
}

class CarServiceModel extends Type0 {
  CarServiceModel({
      required bool success,
      required String error,
      required List<Result> result,}){
    _success = success;
    _error = error;
    _result = result;
}

  CarServiceModel.fromJson(dynamic json) {
    _success = json['success'];
    _error = json['error'];
    if (json['result'] != null) {
      _result = [];
      json['result'].forEach((v) {
        _result.add(Result.fromJson(v));
      });
    }
  }
  bool _success = false;
  String _error = '';
  List<Result> _result = [];

  bool get success => _success;
  String get error => _error;
  List<Result> get result => _result;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = _success;
    map['error'] = _error;
    if (_result != null) {
      map['result'] = _result.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 1
/// name : "NordService"
/// address : "г. Челябинск, ул Пушкина, 17"
/// photo : "https://domen.ru/someimage.jpg"
/// car_brands : [{"id":1,"name":"AUDI"}]
/// work_types : [{"id":1,"name":"Работа с двигателем"}]
/// latitude : 56.77181
/// longitude : 61.89191

class Result {
  Result({
      required int id,
    required String name,
    required String address,
    required String photo,
    required List<CarBrands> carBrands,
    required List<WorkTypes> workTypes,
    required double latitude,
    required double longitude,}){
    _id = id;
    _name = name;
    _address = address;
    _photo = photo;
    _carBrands = carBrands;
    _workTypes = workTypes;
    _latitude = latitude;
    _longitude = longitude;
}

  Result.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _address = json['address'];
    _photo = json['photo'];
    if (json['car_brands'] != null) {
      _carBrands = [];
      json['car_brands'].forEach((v) {
        _carBrands.add(CarBrands.fromJson(v));
      });
    }
    if (json['work_types'] != null) {
      _workTypes = [];
      json['work_types'].forEach((v) {
        _workTypes.add(WorkTypes.fromJson(v));
      });
    }
    _latitude = json['latitude'];
    _longitude = json['longitude'];
  }
  int _id = 0;
  String _name = '';
  String _address = '';
  String _photo = '';
  List<CarBrands> _carBrands = [];
  List<WorkTypes> _workTypes = [];
  double _latitude = 0.0;
  double _longitude = 0.0;

  int get id => _id;
  String get name => _name;
  String get address => _address;
  String get photo => _photo;
  List<CarBrands> get carBrands => _carBrands;
  List<WorkTypes> get workTypes => _workTypes;
  double get latitude => _latitude;
  double get longitude => _longitude;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['address'] = _address;
    map['photo'] = _photo;
    if (_carBrands != null) {
      map['car_brands'] = _carBrands.map((v) => v.toJson()).toList();
    }
    if (_workTypes != null) {
      map['work_types'] = _workTypes.map((v) => v.toJson()).toList();
    }
    map['latitude'] = _latitude;
    map['longitude'] = _longitude;
    return map;
  }

}

/// id : 1
/// name : "Работа с двигателем"

class WorkTypes {
  WorkTypes({
      required int id,
    required String name,}){
    _id = id;
    _name = name;
}

  WorkTypes.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
  }
  int _id = 0;
  String _name = '';

  int get id => _id;
  String get name => _name;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    return map;
  }

}

/// id : 1
/// name : "AUDI"

class CarBrands {
  CarBrands({
      required int id,
      required String name,}){
    _id = id;
    _name = name;
}

  CarBrands.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
  }
  int _id = 0;
  String _name = '';

  int get id => _id;
  String get name => _name;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    return map;
  }

}