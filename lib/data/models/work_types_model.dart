import '../data_sources.dart';

/// success : true
/// error : ""
/// result : [{"id":1,"name":"Работа с мотором"}]

/// Класс коннектор к генератору запросов
class TypeWorkTypesModel implements Type0 {
  static WorkTypesModel fromJson(Map<String, dynamic> json) => WorkTypesModel.fromJson(json);
}

class WorkTypesModel extends Type0{
  WorkTypesModel({
      required bool success,
      required String error,
      required List<Result> result,}){
    _success = success;
    _error = error;
    _result = result;
}

  WorkTypesModel.fromJson(dynamic json) {
    _success = json['success'];
    _error = json['error'];
    if (json['result'] != null) {
      _result = [];
      json['result'].forEach((v) {
        _result.add(Result.fromJson(v));
      });
    }
  }
  bool _success = false;
  String _error = '';
  List<Result> _result = [];

  bool get success => _success;
  String get error => _error;
  List<Result> get result => _result;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = _success;
    map['error'] = _error;
    if (_result != null) {
      map['result'] = _result.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 1
/// name : "Работа с мотором"

class Result {
  Result({
      required int id,
      required String name,}){
    _id = id;
    _name = name;
}

  Result.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
  }
  int _id = 0;
  String _name = '';

  int get id => _id;
  String get name => _name;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    return map;
  }

}