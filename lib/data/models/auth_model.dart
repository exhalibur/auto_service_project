import '../data_sources.dart';

/// Модель Авторизации пользователя
/// success : true
/// error : ""
/// token : "123sgnh-khytud-oiunbs-uthhs"

/// Класс коннектор к генератору запросов
class TypeAuthModel implements Type0 {
  static AuthModel fromJson(Map<String, dynamic> json) => AuthModel.fromJson(json);
}

class AuthModel extends Type0{
  AuthModel({
      required bool success,
      required String error,
      required String token,}){
    _success = success;
    _error = error;
    _token = token;
}

  AuthModel.fromJson(dynamic json) {
    _success = json['success'];
    _error = json['error'];
    _token = json['token'] ?? '';
  }
  bool _success = false;
  String _error = '';
  String _token = '';

  bool get success => _success;
  String get error => _error;
  String get token => _token;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = _success;
    map['error'] = _error;
    map['token'] = _token;
    return map;
  }

}