import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:flutter_blank/core/dictionaries/constants.dart';
import 'package:flutter_blank/core/singletons/local_storage.dart';
import 'package:flutter_blank/core/singletons/urls.dart';
import 'package:flutter_blank/data/models/auth_model.dart';
import 'package:flutter_blank/data/models/basic_model.dart';
import 'package:flutter_blank/domain/data_sources.dart';

import '../core/error/failure.dart';
import 'models/car_service_model.dart';
import 'models/work_types_model.dart';

/// Класс коннектор для генератора запросов
class Type0 {
  static T fromJson<T extends Type0>(Map<String, dynamic> json) =>
      _fromJsons[T]!(json) as T;

  static const Map<Type, Constructor<Type0>> _fromJsons = {
    /// Список коннектора моделей к их конструкторам
    BasicModel: TypeBasicModel.fromJson,
    AuthModel: TypeAuthModel.fromJson,
    WorkTypesModel: TypeWorkTypesModel.fromJson,
    CarServiceModel: TypeCarServiceModel.fromJson,
  };
}

/// Модуль сборки запросов к серверу
class DataSources{
  /// --------------------------------------------------------------------------
  static Future<Either<Failure, AuthModel>> login (LoginParams params) async {
    final Params loginParams = Params(
        method: Methods.post,
        url: Urls.login,
        body: {
          'phone': params.login,
          'password': params.password,
        }
    );
    return SendRequest<AuthModel>().getResponse(loginParams);
  }
  /// --------------------------------------------------------------------------
  static Future<Either<Failure, AuthModel>> registration (RegistrationParams params) async {
    final Params loginParams = Params(
        method: Methods.post,
        url: Urls.registration,
        body: {
          'phone': params.login,
          'password': params.password,
          'email': params.email,
        }
    );
    return SendRequest<AuthModel>().getResponse(loginParams);
  }
  /// --------------------------------------------------------------------------
  static Future<Either<Failure, WorkTypesModel>> workTypes() async {
    final Params workTypeParams = Params(
      method: Methods.get,
      url: Urls.workTypes,
    );
    return SendRequest<WorkTypesModel>().getResponse(workTypeParams);
  }
  /// --------------------------------------------------------------------------
  static Future<Either<Failure, WorkTypesModel>> carBrands () async {
    final Params carBrandsParams = Params(
      method: Methods.get,
      url: Urls.carBrands,
    );
    return SendRequest<WorkTypesModel>().getResponse(carBrandsParams);
  }
  /// --------------------------------------------------------------------------
  static Future<Either<Failure, CarServiceModel>> getService (GetServicesParams params) async {
    final Params carBrandsParams = Params(
      method: Methods.post,
      url: Urls.services,
      body: {
        "car_brand_id": params.carBrandId,
        "work_type_id": params.workTypeId,
        "latitude": params.latitude,
        "longitude": params.longitude
      }
    );
    return SendRequest<CarServiceModel>().getResponse(carBrandsParams);
  }
}

class LoginParams{
  final String login;
  final String password;
  LoginParams({required this.login, required this.password});
}

class RegistrationParams{
  final String login;
  final String password;
  final String email;
  RegistrationParams({
    required this.login,
    required this.password,
    required this.email,
  });
}

class GetServicesParams{
  final int carBrandId;
  final int workTypeId;
  final double latitude;
  final double longitude;
  GetServicesParams({
    required this.carBrandId,
    required this.workTypeId,
    required this.latitude,
    required this.longitude,
  });
}