part of 'language_bloc.dart';

@immutable
abstract class LanguageState extends Equatable {
  @override
  List<Object?> get props => [];
}

class LanguageRus extends LanguageState {}
class LanguageEng extends LanguageState {}
class LanguageLat extends LanguageState {}
