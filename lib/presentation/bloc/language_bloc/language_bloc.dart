
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../core/dictionaries/constants.dart';
import '../../../core/singletons/languages.dart';
import '../../../core/singletons/local_storage.dart';

part 'language_event.dart';
part 'language_state.dart';

class LanguageBloc extends Bloc<LanguageEvent, LanguageState> {
  LanguageBloc() : super(LanguageLat()) {
    on<ChangeLang>((event, emit) async {
      print('Im change on ${event.lang.name}');
      Language.changeState(event.lang);
      if(event.lang == Languages.rus){
        emit(LanguageRus());
      }else if(event.lang == Languages.eng){
        emit(LanguageEng());
      }else if(event.lang == Languages.lat){
        emit(LanguageLat());
      }
    });
  }
}
