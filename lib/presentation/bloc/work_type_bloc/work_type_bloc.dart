import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter_blank/core/dictionaries/constants.dart';
import 'package:flutter_blank/core/error/failure.dart';
import 'package:flutter_blank/core/singletons/local_storage.dart';
import 'package:flutter_blank/data/data_sources.dart';
import 'package:flutter_blank/data/models/work_types_model.dart';
import 'package:meta/meta.dart';

part 'work_type_event.dart';
part 'work_type_state.dart';

class WorkTypeBloc extends Bloc<WorkTypeEvent, WorkTypeState> {
  WorkTypeBloc() : super(WorkTypeLoadingState()) {
    on<WorkTypeGetInfoEvent>((event, emit) async {
      emit(WorkTypeLoadingState());
      try {
        WorkTypesModel cache = WorkTypesModel.fromJson(
            jsonDecode(LocalStorage.getString(AppConstants.WORK_TYPES)));
        emit(WorkTypeDoneState(workTypes: cache));
      } catch (e) {
        var re = await DataSources.workTypes();
        late String tempCache;
        re.fold((l) {
          if (l is UnAuthFailure) {
            emit(WorkTypeUnAuthState());
          } else if (l is ConnectionFailure) {
            emit(WorkTypeNetworkErrorState());
          } else if (l is ServerFailure) {
            emit(WorkTypeFailedState(message: l.error));
          }
        }, (r) {
          tempCache = jsonEncode(r.toJson());
          emit(WorkTypeDoneState(workTypes: r));
        });
        await LocalStorage.setString(AppConstants.WORK_TYPES, tempCache);
      }
    });
  }
}
