part of 'work_type_bloc.dart';

@immutable
abstract class WorkTypeState {}

class WorkTypeLoadingState extends WorkTypeState {}
class WorkTypeFailedState extends WorkTypeState {
  final String message;
  WorkTypeFailedState({required this.message});
}
class WorkTypeDoneState extends WorkTypeState {
  final WorkTypesModel workTypes;
  WorkTypeDoneState({required this.workTypes});
}
class WorkTypeUnAuthState extends WorkTypeState {}
class WorkTypeNetworkErrorState extends WorkTypeState {}
