part of 'work_type_bloc.dart';

@immutable
abstract class WorkTypeEvent {}

class WorkTypeGetInfoEvent extends WorkTypeEvent{}