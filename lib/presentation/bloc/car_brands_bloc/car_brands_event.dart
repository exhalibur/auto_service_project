part of 'car_brands_bloc.dart';

@immutable
abstract class CarBrandsEvent {}

class CarBrandsGetInfoEvent extends CarBrandsEvent{}
