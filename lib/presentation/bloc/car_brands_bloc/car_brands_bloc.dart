import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter_blank/data/models/work_types_model.dart';
import 'package:meta/meta.dart';

import '../../../core/dictionaries/constants.dart';
import '../../../core/error/failure.dart';
import '../../../core/singletons/local_storage.dart';
import '../../../data/data_sources.dart';

part 'car_brands_event.dart';
part 'car_brands_state.dart';

class CarBrandsBloc extends Bloc<CarBrandsEvent, CarBrandsState> {
  CarBrandsBloc() : super(CarBrandsLoadingState()) {
    on<CarBrandsGetInfoEvent>((event, emit) async {
      emit(CarBrandsLoadingState());
      try {
        WorkTypesModel cache = WorkTypesModel.fromJson(
            jsonDecode(LocalStorage.getString(AppConstants.CAR_BRANDS)));
        emit(CarBrandsDoneState(carBrands: cache));
      } catch (e) {
        var re = await DataSources.carBrands();
        late String tempCache;
        re.fold((l) {
          if (l is UnAuthFailure) {
            emit(CarBrandsUnAuthState());
          } else if (l is ConnectionFailure) {
            emit(CarBrandsNetworkErrorState());
          } else if (l is ServerFailure) {
            emit(CarBrandsFailedState(message: l.error));
          }
        }, (r) {
          tempCache = jsonEncode(r.toJson());
          emit(CarBrandsDoneState(carBrands: r));
        });
        await LocalStorage.setString(
            AppConstants.CAR_BRANDS, tempCache);
      }
    });
  }
}
