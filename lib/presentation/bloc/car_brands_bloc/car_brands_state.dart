part of 'car_brands_bloc.dart';

@immutable
abstract class CarBrandsState {}

class CarBrandsLoadingState extends CarBrandsState {}
class CarBrandsFailedState extends CarBrandsState {
  final String message;
  CarBrandsFailedState({required this.message});
}
class CarBrandsDoneState extends CarBrandsState {
  final WorkTypesModel carBrands;
  CarBrandsDoneState({required this.carBrands});
}
class CarBrandsUnAuthState extends CarBrandsState {}
class CarBrandsNetworkErrorState extends CarBrandsState {}
