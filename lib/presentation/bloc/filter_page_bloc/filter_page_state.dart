part of 'filter_page_bloc.dart';

@immutable
abstract class FilterPageState {}

class FilterPageWorkType extends FilterPageState {}
class FilterPageCarBrand extends FilterPageState {}
