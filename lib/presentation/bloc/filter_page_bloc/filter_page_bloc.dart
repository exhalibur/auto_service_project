import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_blank/presentation/pages/main/filter/filter_page.dart';
import 'package:meta/meta.dart';

part 'filter_page_event.dart';
part 'filter_page_state.dart';

class FilterPageBloc extends Bloc<FilterPageEvent, FilterPageState> {
  FilterPageBloc() : super(FilterPageCarBrand()) {
    on<ChangeFilterPage>((event, emit) {
      if(FilterPage.carBrand == event.page){
        emit(FilterPageCarBrand());
      }else if(FilterPage.workType == event.page){
        emit(FilterPageWorkType());
      }
    });
  }
}
