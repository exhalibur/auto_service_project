part of 'filter_page_bloc.dart';

@immutable
abstract class FilterPageEvent {}

class ChangeFilterPage extends FilterPageEvent{
  final FilterPage page;
  ChangeFilterPage({required this.page});
}