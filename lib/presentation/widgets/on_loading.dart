import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

import '../../core/error/failure.dart';

onLoading<T>(context, {
  required Future<Either<Failure, T>> futureFunction,
  required Function onComplete,
}) async {
  BuildContext dialogContext = context;
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      dialogContext = context;
      return Dialog(
          child: SizedBox(
            width: MediaQuery.of(context).size.width / 2,
            height: 150,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: const [
                CircularProgressIndicator(),
                Text("Ожидание..."),
              ],
            ),
          )
      );
    },
  );
  var re = await futureFunction;
  Navigator.of(dialogContext).pop();
  onComplete(re);
}