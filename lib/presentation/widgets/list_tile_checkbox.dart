/// Модуль непрокручеваемого списка ListTile с CheckBox's

import 'package:flutter/material.dart';

import '../../core/singletons/theme.dart';

class ListTileCheckBox<T> extends StatefulWidget {
  final Color activeColor;
  final Color inactiveColor;
  final GroupSwitchController<T> controller;
  final List<String> titles;
  final List<T> values;
  const ListTileCheckBox({Key? key,
    this.activeColor = Colors.blue,
    this.inactiveColor = Colors.grey,
    required this.controller,
    required this.titles,
    required this.values
  }) : super(key: key);

  @override
  _ListTileCheckBoxState createState() => _ListTileCheckBoxState();
}

class _ListTileCheckBoxState extends State<ListTileCheckBox> {

  List<bool>? initialValues;
  bool firstTry = true;

  @override
  Widget build(BuildContext context) {
    if(firstTry){
      initialValues = List.generate(widget.titles.length, (index) {
        if(widget.controller.initSelectValue.contains(widget.values[index])){
          widget.controller.selectValue = widget.controller.initSelectValue;
          return true;
        }
        return false;
      });
      firstTry = false;
    }
    if(widget.titles.length != widget.values.length){
      return Card(
        child: ListTile(
          title: Text('titles.length != values.length)'),
        ),
      );
    }
    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: widget.titles.length,
      itemBuilder: (BuildContext context, int index) {
        return Card(
          child: SwitchListTile(
            activeColor: widget.activeColor,
            selectedTileColor: widget.activeColor,
            onChanged: (bool isOn) {
              setState(() {
                initialValues![index] = !initialValues![index];
              });
              if (isOn){
                widget.controller.setItem(widget.values[index]);
              } else {
                widget.controller.deleteItem(widget.values[index]);
              }
            },
            value: widget.values[index] == widget.controller.selectValue,
            title: Builder(
              builder: (context){
                Color currentColor = ThemeApp.textColorMain;
                if(widget.controller.selectValue == widget.values[index]){
                  currentColor = ThemeApp.darkBlue;
                }else{
                  currentColor = ThemeApp.textColorMain;
                }
                return Text(widget.titles[index],
                  style: TextStyle(
                      color: currentColor
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }
}

class GroupSwitchController<T> extends ChangeNotifier{
  T? selectValue;
  List<T> initSelectValue = [];

  void setItem(value){
    selectValue = value;
    notifyListeners();
  }

  void deleteItem(value){
    selectValue = null;
    notifyListeners();
  }
}