/// Модуль Кастомного Popup окна
import 'package:flutter/material.dart';

class CustomAlertDialog extends StatelessWidget{

  final String title;
  final String description;
  Widget? additionalContent;
  List<Widget>? actions;

  CustomAlertDialog({Key? key,
    required this.title,
    required this.description,
    this.additionalContent,
    this.actions,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title,
        style: const TextStyle(
          fontSize: 22,
          fontWeight: FontWeight.bold,
        ),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(description),
          additionalContent ?? const SizedBox.shrink(),
        ],
      ),
      actions: actions ?? <Widget>[
        TextButton(
          onPressed: (){
            Navigator.pop(context);
          },
          child: const Text('OK'),
        ),
      ],
    );
  }

}