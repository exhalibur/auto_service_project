import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ionicons/ionicons.dart';
import 'package:provider/src/provider.dart';

import '../../../core/dictionaries/constants.dart';
import '../../../core/singletons/languages.dart';
import '../../../core/singletons/local_storage.dart';
import '../../../core/singletons/theme.dart';
import '../../bloc/language_bloc/language_bloc.dart';

class SettingsLanguage extends StatelessWidget{
  
  const SettingsLanguage({
    Key? key
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ThemeApp.isDark ? null : ThemeApp.blue,
        title: Text(Language.language,
          style: TextStyle(
            color: ThemeApp.textColorMain,
          ),
        ),
        leading: IconButton(
          onPressed: (){
            Navigator.of(context).pop(true);
          },
          icon: Icon(Icons.arrow_back, color: ThemeApp.textColorMain,),
        ),
      ),
      body: ListView.builder(
          itemCount: Languages.values.length,
          itemBuilder: (context, index){
            return Column(
              children: [
                ListTile(
                  onTap: () async {
                    await LocalStorage.setString(
                      AppConstants.LANGUAGE,
                      Languages.values[index].name,
                    );
                    context.read<LanguageBloc>().add(
                      ChangeLang(
                        lang: Languages.values[index],
                      ),
                    );
                  },
                  title: Text('${Languages.values[index].name}',
                    style: const TextStyle(
                        fontSize: 18
                    ),
                  ),
                  subtitle: Text(Languages.values[index].name),
                  trailing: Icon(
                    LocalStorage.getString(AppConstants.LANGUAGE) == Languages.values[index].name ?
                    Ionicons.checkmark_done_outline : null,
                    size: 35,
                    color: ThemeApp.blue,
                  ),
                ),
                const Divider(),
              ],
            );
          }
      ),
    );
  }

}