import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_blank/core/singletons/languages.dart';

import '../../../core/dictionaries/constants.dart';
import '../../../core/dictionaries/routes.dart';
import '../../../core/error/custom_errors.dart';
import '../../../core/error/failure.dart';
import '../../../core/singletons/local_storage.dart';
import '../../../core/singletons/theme.dart';
import '../../../data/data_sources.dart';
import '../../../data/models/auth_model.dart';
import '../../../domain/formatters/mobile_formater.dart';
import '../../widgets/custom_alert_dialog.dart';
import '../../widgets/on_loading.dart';

class Registration extends StatelessWidget{

  final TextEditingController _controllerPhone = TextEditingController();
  final TextEditingController _controllerEmail = TextEditingController();

  final TextEditingController _controllerPassword = TextEditingController();
  final TextEditingController _controllerConfirmPassword = TextEditingController();

  final MobileFormater mobileFormater = MobileFormater();
  RegExp regExp = RegExp(r'^371[0-9]{8}$');
  RegExp exp = RegExp(r"[()-]+");

  final _formKey = GlobalKey<FormState>();

  Registration({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: ThemeApp.textColorMain,),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(ThemeApp.backgroundImage),
              fit: BoxFit.contain,
              alignment: Alignment.topCenter,
            )
        ),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.all(10.0),
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(Language.registration, style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),),
                        Text(Language.gladToMeet, style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),),
                      ],
                    ),
                    // margin: const EdgeInsets.all(10.0),
                    // child: const Text('Авторизация', style: TextStyle(fontSize: 30),),
                  ),
                  Container(
                      padding: const EdgeInsets.all(5.0),
                      child: Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.all(10.0),
                            child: TextFormField(
                              inputFormatters: [
                                FilteringTextInputFormatter.digitsOnly,
                                mobileFormater,
                              ],
                              maxLength: 11,
                              style: const TextStyle(fontSize: 16),
                              keyboardType: TextInputType.phone,
                              controller: _controllerPhone,
                              validator: (val) {
                                val = '371${val!.replaceAll(exp, '')}';
                                if (val.isEmpty) {
                                  return Errors.emptyString;
                                }else if(!regExp.hasMatch(val)){
                                  return Errors.unValidFormatPhone;
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                counterText: '',
                                counterStyle: TextStyle(fontSize: 0),
                                prefixIcon:Padding(padding: EdgeInsets.all(10),
                                  child:  Text("+371", style: TextStyle(fontSize: 16),),),
                                prefixIconConstraints: BoxConstraints(minWidth: 0, minHeight: 0),
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.teal)),
                                labelText: Language.phone,
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10.0),
                            child: TextFormField(
                              controller: _controllerEmail,
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return Errors.emptyString;
                                }
                                return null;
                              },
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.teal)),
                                labelText: 'E-mail*',
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10.0),
                            child: TextFormField(
                              controller: _controllerPassword,
                              obscureText: true,
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return Errors.emptyString;
                                } else if(value.length < 8){
                                  return Errors.shortPassword;
                                }else if(_controllerPassword.text != _controllerConfirmPassword.text){
                                  return Errors.unMatchPassword;
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.teal)),
                                labelText: Language.password,
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10.0),
                            child: TextFormField(
                              controller: _controllerConfirmPassword,
                              obscureText: true,
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return Errors.emptyString;
                                } else if(value.length < 8){
                                  return Errors.shortPassword;
                                }else if(_controllerPassword.text != _controllerConfirmPassword.text){
                                  return Errors.unMatchPassword;
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.teal)),
                                labelText: Language.confirmPassword,
                              ),
                            ),
                          ),

                          Container(
                            alignment: Alignment.centerRight,
                            margin: const EdgeInsets.all(10.0),
                            child: MaterialButton(
                              minWidth: MediaQuery.of(context).size.width / 2,
                              height: 50.0,
                              onPressed: (){
                                if (_formKey.currentState!.validate()) {
                                  onLoading<AuthModel>(context,
                                    futureFunction: DataSources.registration(RegistrationParams(
                                      login: '371${_controllerPhone.text.replaceAll(exp, '')}',
                                      password: _controllerPassword.text.trim(),
                                      email: _controllerEmail.text,
                                    )),
                                    onComplete: (Either<Failure, AuthModel> re){
                                      re.fold(
                                          (l){
                                            showDialog<String>(
                                                context: context,
                                                builder: (BuildContext context) => CustomAlertDialog(
                                                  title: Language.failure,
                                                  description: l.error,
                                                )
                                            );
                                          },
                                          (r) async {
                                            await LocalStorage.setString(AppConstants.TOKEN, r.token);
                                            Navigator.of(context).pushNamedAndRemoveUntil(AppRoutes.home, (route) => false);
                                          }
                                      );
                                    },
                                  );
                                }
                              },
                              color: ThemeApp.blue,
                              child: Text(Language.registrationUser,style: TextStyle(fontSize: 18, color: ThemeApp.white),),
                            ),
                          ),
                        ],
                      )
                  ),
                ]
            ),
          ),
        ),
      ),
    );
  }
}