/// Страница восстановления пароля

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_blank/core/singletons/languages.dart';

import '../../../core/error/custom_errors.dart';
import '../../../core/singletons/theme.dart';
import '../../../domain/formatters/mobile_formater.dart';

class RestorePasswordPage extends StatelessWidget{

  final TextEditingController _controllerPhone = TextEditingController();
  final TextEditingController _controllerPassword = TextEditingController();
  final TextEditingController _controllernewPassword = TextEditingController();
  final MobileFormater mobileFormater = MobileFormater();
  final _formKey = GlobalKey<FormState>();
  RegExp regExp = RegExp(r'^7[0-9]{10}$');
  RegExp exp = RegExp(r"[()-]+");

  RestorePasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      body: Container(
        margin: const EdgeInsets.only(top: 80),
        decoration: BoxDecoration(
            color: Colors.transparent,
            image: DecorationImage(
                image: AssetImage(ThemeApp.backgroundImage),
                fit: BoxFit.cover,
                alignment: Alignment.centerLeft
            )
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.all(10.0),
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(Language.passwordRecovery, style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),),
                  ],
                ),
              ),
              Form(
                key: _formKey,
                child: Container(
                    padding: const EdgeInsets.all(5.0),
                    child: Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.all(10.0),
                          child: TextFormField(
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly,
                              mobileFormater,
                            ],
                            maxLength: 14,
                            style: const TextStyle(fontSize: 16),
                            keyboardType: TextInputType.phone,
                            controller: _controllerPhone,
                            validator: (val) {
                              val = '371${val!.replaceAll(exp, '')}';
                              if (val.isEmpty) {
                                return Errors.emptyString;
                              }else if(!regExp.hasMatch(val)){
                                return Errors.unValidFormatPhone;
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              counterText: '',
                              counterStyle: TextStyle(fontSize: 0),
                              prefixIcon:Padding(padding: EdgeInsets.all(10),
                                child:  Text("+371", style: TextStyle(fontSize: 16),),),
                              prefixIconConstraints: BoxConstraints(minWidth: 0, minHeight: 0),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.teal)),
                              labelText: Language.phone,
                            ),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.all(10.0),
                          child: TextFormField(
                            obscureText: true,
                            controller: _controllerPassword,
                            validator: (val) {
                              if(val!.isEmpty){
                                return Errors.emptyString;
                              }else if(val != _controllernewPassword.text){
                                return Errors.unMatchPassword;
                              }else{
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.teal)),
                              labelText: Language.password,
                            ),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.all(10.0),
                          child: TextFormField(
                            obscureText: true,
                            controller: _controllernewPassword,
                            validator: (val) {
                              if(val!.isEmpty){
                                return Errors.emptyString;
                              }else if(val != _controllerPassword.text){
                                return Errors.unMatchPassword;
                              }else{
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.teal)),
                              labelText: Language.confirmPassword,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width - 30,
                          child: MaterialButton(
                            color: ThemeApp.blue,
                            padding: const EdgeInsets.all(10.0),
                            onPressed: (){
                              if(_formKey.currentState!.validate()){
                                // TODO: Восстановление пароля
                              }
                            },
                            child: Text(Language.restorePassword, style: TextStyle(fontSize: 18),),
                          ),
                        ),
                      ],
                    )
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}