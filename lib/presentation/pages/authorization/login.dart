import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_blank/core/dictionaries/constants.dart';
import 'package:flutter_blank/core/dictionaries/routes.dart';
import 'package:flutter_blank/core/singletons/languages.dart';
import 'package:flutter_blank/core/singletons/local_storage.dart';
import 'package:flutter_blank/data/data_sources.dart';
import 'package:flutter_blank/data/models/auth_model.dart';
import 'package:flutter_blank/presentation/pages/authorization/registration.dart';
import 'package:flutter_blank/presentation/widgets/on_loading.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ionicons/ionicons.dart';

import '../../../core/error/custom_errors.dart';
import '../../../core/error/failure.dart';
import '../../../core/singletons/theme.dart';
import '../../../domain/formatters/mobile_formater.dart';
import '../../widgets/custom_alert_dialog.dart';
import '../settings/language_select.dart';
import 'restore_password.dart';

class Login extends StatelessWidget{

  final TextEditingController _controllerPhone = TextEditingController();
  final MobileFormater mobileFormater = MobileFormater();
  final TextEditingController _controllerPass = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  RegExp regExp = RegExp(r'^371[0-9]{8}$');
  RegExp exp = RegExp(r"[()-]+");

  Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        actions: [
          IconButton(onPressed: (){
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => SettingsLanguage())
            );
          }, icon: Icon(Ionicons.earth_outline, color: ThemeApp.black,))
        ],
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            color: Colors.transparent,
            image: DecorationImage(
                image: AssetImage(ThemeApp.backgroundImage),
                fit: BoxFit.cover,
                alignment: Alignment.centerLeft
            )
        ),
        child: Container(
          height: MediaQuery.of(context).size.height,
          color: ThemeApp.white.withOpacity(0.1),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const SizedBox.shrink(),
              Form(
                key: _formKey,
                child: Container(
                    padding: const EdgeInsets.all(5.0),
                    child: Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.all(10.0),
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(Language.authorization, style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),),
                              Text(Language.welcome, style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),),
                            ],
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.all(10.0),
                          child: TextFormField(
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly,
                              mobileFormater,
                            ],
                            maxLength: 11,
                            style: const TextStyle(fontSize: 16),
                            keyboardType: TextInputType.phone,
                            controller: _controllerPhone,
                            validator: (val) {
                              val = '371${val!.replaceAll(exp, '')}';
                              if (val.isEmpty) {
                                return Errors.emptyString;
                              }else if(!regExp.hasMatch(val)){
                                return Errors.unValidFormatPhone;
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              counterText: '',
                              counterStyle: TextStyle(fontSize: 0),
                              prefixIcon:Padding(padding: EdgeInsets.all(10),
                                child:  Text("+371", style: TextStyle(fontSize: 16),),),
                              prefixIconConstraints: BoxConstraints(minWidth: 0, minHeight: 0),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.teal)),
                              labelText: Language.phone,
                            ),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.symmetric(horizontal: 10.0),
                          child: TextFormField(
                            obscureText: true,
                            controller: _controllerPass,
                            validator: (val) => val!.isEmpty ? Errors.emptyString : null,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.teal)),
                              labelText: Language.password,
                            ),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.symmetric(horizontal: 7.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              TextButton(
                                onPressed: (){
                                  Navigator.of(context).push(
                                      MaterialPageRoute(builder: (context) => RestorePasswordPage())
                                  );
                                },
                                child: Text(Language.forgotPassword),
                              )
                            ],
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerRight,
                          margin: const EdgeInsets.symmetric(horizontal: 10.0),
                          child: MaterialButton(
                            minWidth: MediaQuery.of(context).size.width,
                            height: 50.0,
                            onPressed: () async {
                              if (_formKey.currentState!.validate()) {
                                onLoading<AuthModel>(context,
                                  futureFunction: DataSources.login(LoginParams(
                                    login: '371${_controllerPhone.text.replaceAll(exp, '')}',
                                    password: _controllerPass.text.trim(),
                                  )),
                                  onComplete: (Either<Failure, AuthModel> re){
                                    re.fold(
                                      (l){
                                        showDialog<String>(
                                            context: context,
                                            builder: (BuildContext context) => CustomAlertDialog(
                                              title: Language.failure,
                                              description: l.error,
                                            )
                                        );
                                      },
                                      (r) async {
                                        await LocalStorage.setString(AppConstants.TOKEN, r.token);
                                        Navigator.of(context).pushNamedAndRemoveUntil(AppRoutes.home, (route) => false);
                                      }
                                    );
                                  },
                                );
                              }
                            },
                            color: ThemeApp.blue,
                            child: Text(Language.enter,
                              style: TextStyle(fontSize: 18, color: ThemeApp.white),),
                          ),
                        ),
                      ],
                    )
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(Language.noAccount,
                      style: TextStyle(
                          fontSize: 16
                      ),
                    ),
                    TextButton(
                      onPressed: (){
                        Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => Registration())
                        );
                      },
                      child: Text(Language.register,
                        style: TextStyle(
                            fontSize: 16
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}