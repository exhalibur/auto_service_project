import 'package:flutter/material.dart';
import 'package:flutter_blank/core/dictionaries/constants.dart';
import 'package:flutter_blank/core/dictionaries/routes.dart';
import 'package:flutter_blank/core/singletons/local_storage.dart';
import 'package:flutter_blank/data/data_sources.dart';
import 'package:flutter_blank/presentation/bloc/car_brands_bloc/car_brands_bloc.dart';
import 'package:flutter_blank/presentation/bloc/filter_page_bloc/filter_page_bloc.dart';
import 'package:flutter_blank/presentation/bloc/work_type_bloc/work_type_bloc.dart';
import 'package:flutter_blank/presentation/widgets/on_loading.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_osm_plugin/flutter_osm_plugin.dart';
import 'package:ionicons/ionicons.dart';

import '../../../core/singletons/languages.dart';
import '../../../core/singletons/theme.dart';
import '../../../data/models/car_service_model.dart';
import 'filter/filter_page.dart';

class MainPage extends StatelessWidget {

  MapController controller = MapController(
    initMapWithUserPosition: true,
  );

  List<Result> currentService = [];
  ValueNotifier<bool> isLoading = ValueNotifier<bool>(true);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: Container(
          margin: const EdgeInsets.all(5.0),
          child: CircleAvatar(
            radius: 30,
            backgroundColor: ThemeApp.grey.withOpacity(0.4),
            child: IconButton(
              onPressed: () async {
                await LocalStorage.setString(AppConstants.TOKEN, '');
                Navigator.of(context).pushNamedAndRemoveUntil(
                  AppRoutes.login, (route) => false
                );
              },
              icon: Icon(Icons.exit_to_app,
                color: ThemeApp.white,
              ),
            ),
          ),
        ),
      ),
      body: OSMFlutter(
        onGeoPointClicked: (location) {
          Result service = currentService.firstWhere((element) =>
            element.latitude == location.latitude && element.longitude == location.longitude);
          _showBottomSheet(context, service);
        },
        trackMyPosition: true,
        userLocationMarker: UserLocationMaker(
          personMarker: const MarkerIcon(
            icon: Icon(
              Icons.person_pin_circle,
              color: Colors.blue,
              size: 100,
            ),
          ),
          directionArrowMarker: const MarkerIcon(
            icon: Icon(
              Icons.double_arrow,
              color: Colors.yellow,
              size: 80,
            ),
          ),
        ),
        androidHotReloadSupport: true,
        onMapIsReady: (isReady) async {
          if (isReady) {
            GeoPoint myPoint = await controller.myLocation();
            await controller.currentLocation();
            await controller.zoomToBoundingBox(
              BoundingBox(
                east: myPoint.longitude + 0.01,
                north: myPoint.latitude + 0.01,
                south: myPoint.latitude - 0.01,
                west: myPoint.longitude - 0.01,
              ),
              paddinInPixel: 200,
            );
            isLoading.value = false;
          }
        },
        controller: controller,
      ),
      floatingActionButton: Container(
        width: MediaQuery.of(context).size.width - 30,
        height: 55,
        child: MaterialButton(
          onPressed: (){
            Navigator.of(context)
                .push(MaterialPageRoute(
                builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                    create: (context) => FilterPageBloc(),
                  ),
                  BlocProvider(
                    create: (context) =>
                    CarBrandsBloc()..add(CarBrandsGetInfoEvent()),
                  ),
                  BlocProvider(
                    create: (context) =>
                    WorkTypeBloc()..add(WorkTypeGetInfoEvent()),
                  ),
                ], child: Filter())))
                .then((value) async {
              if (value != null) {
                GeoPoint location = await controller.myLocation();
                await controller.currentLocation();
                await controller.zoomToBoundingBox(
                  BoundingBox(
                    east: location.longitude + 0.1,
                    north: location.latitude + 0.1,
                    south: location.latitude - 0.1,
                    west: location.longitude - 0.1,
                  ),
                  paddinInPixel: 200,
                );
                print(
                    'workType: ${value['workType']}\ncarBrand: ${value['carBrand']}');
                onLoading<CarServiceModel>(
                  context,
                  futureFunction: DataSources.getService(GetServicesParams(
                    carBrandId: int.parse(value['carBrand'].toString()),
                    workTypeId: int.parse(value['workType'].toString()),
                    latitude: location.latitude,
                    longitude: location.longitude,
                  )),
                  onComplete: (re) {
                    re.fold((l) {

                    }, (CarServiceModel r) {
                      currentService = r.result;
                      r.result.forEach((element) {
                        controller.addMarker(GeoPoint(latitude: element.latitude, longitude: element.longitude));
                      });
                    });
                  },
                );
              }
            });
          },
          color: ThemeApp.darkBlue,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Icon(Ionicons.search_outline, color: ThemeApp.white,),
              Text(Language.findServices,
                style: TextStyle(
                  fontSize: 16,
                  color: ThemeApp.white
                ),
              ),
              SizedBox.shrink(),
            ],
          ),
        ),
      ),
    );
  }

  void _showBottomSheet(BuildContext context, Result service) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      builder: (context) {
        return GestureDetector(
          onTap: () => Navigator.of(context).pop(),
          child: Container(
            color: Color.fromRGBO(0, 0, 0, 0.001),
            child: GestureDetector(
              onTap: () {},
              child: DraggableScrollableSheet(
                initialChildSize: 0.8,
                minChildSize: 0.2,
                maxChildSize: 0.9,
                builder: (_, controller) {
                  return Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(25.0),
                        topRight: const Radius.circular(25.0),
                      ),
                    ),
                    child: Column(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            color: ThemeApp.blue,
                            borderRadius: BorderRadius.only(
                              topLeft: const Radius.circular(25.0),
                              topRight: const Radius.circular(25.0),
                            ),
                          ),
                          child: Icon(
                            Icons.remove,
                            color: Colors.grey[600],
                          ),
                        ),
                        Expanded(
                          child: SingleChildScrollView(
                            child: Column(
                              children: [
                                Container(
                                  padding: EdgeInsets.all(15.0),
                                  width: MediaQuery.of(context).size.width,
                                  color: ThemeApp.blue,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Row(
                                        children: [
                                          Expanded(
                                            child: MaterialButton(
                                              onPressed: (){},
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Icon(Ionicons.bookmark_outline, color: ThemeApp.white),
                                                  Text(Language.save,
                                                    style: TextStyle(
                                                      color: ThemeApp.white
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            child: MaterialButton(
                                              onPressed: (){},
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Icon(Ionicons.map_outline, color: ThemeApp.white,),
                                                  Text(Language.route,
                                                    style: TextStyle(
                                                        color: ThemeApp.white
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Text(service.name,
                                        style: TextStyle(
                                          fontSize: 22,
                                          fontWeight: FontWeight.bold,
                                          color: ThemeApp.white
                                        ),
                                      ),
                                      Text(service.address,
                                        style: TextStyle(
                                          fontSize: 18,
                                          color: ThemeApp.white
                                        ),
                                      ),
                                      Text('${service.latitude} ${service.longitude}',
                                        style: TextStyle(
                                            fontSize: 18,
                                            color: ThemeApp.white
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                AspectRatio(aspectRatio: 16/9,
                                  child: Image.network(service.photo),
                                ),
                                Text(Language.carBrand,
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: ThemeApp.textColorMain,
                                  ),
                                ),
                                SizedBox(
                                  height: 60,
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: service.carBrands.length,
                                    itemBuilder: (context, index){
                                      return Center(
                                        child: Container(
                                          padding: EdgeInsets.all(4.0),
                                          margin: EdgeInsets.all(4.0),
                                          decoration: BoxDecoration(
                                            color: ThemeApp.blue,
                                            borderRadius: BorderRadius.circular(25.0),
                                          ),
                                          child: Text(service.carBrands[index].name,
                                            style: TextStyle(
                                              fontSize: 14,
                                              color: ThemeApp.white
                                            ),
                                          ),
                                        ),
                                      );
                                    }
                                  ),
                                ),
                                Text(Language.workTypes,
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: ThemeApp.textColorMain,
                                  ),
                                ),
                                SizedBox(
                                  height: 60,
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                      itemCount: service.workTypes.length,
                                      itemBuilder: (context, index){
                                        return Center(
                                          child: Container(
                                            padding: EdgeInsets.all(4.0),
                                            margin: EdgeInsets.all(4.0),
                                            decoration: BoxDecoration(
                                              color: ThemeApp.blue,
                                              borderRadius: BorderRadius.circular(25.0),
                                            ),
                                            child: Text(service.workTypes[index].name,
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  color: ThemeApp.white
                                              ),
                                            ),
                                          ),
                                        );
                                      }
                                  ),
                                ),

                              ],
                            ),
                          )
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
          ),
        );
      },
    );
  }

}
