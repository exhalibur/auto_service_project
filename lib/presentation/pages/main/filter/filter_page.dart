import 'package:flutter/material.dart';
import 'package:flutter_blank/core/singletons/languages.dart';
import 'package:flutter_blank/core/singletons/theme.dart';
import 'package:flutter_blank/data/models/work_types_model.dart';
import 'package:flutter_blank/presentation/bloc/car_brands_bloc/car_brands_bloc.dart';
import 'package:flutter_blank/presentation/bloc/filter_page_bloc/filter_page_bloc.dart';
import 'package:flutter_blank/presentation/bloc/work_type_bloc/work_type_bloc.dart';
import 'package:flutter_blank/presentation/widgets/list_tile_checkbox.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ionicons/ionicons.dart';

enum FilterPage{workType, carBrand}
class Filter extends StatefulWidget {
  const Filter({Key? key}) : super(key: key);

  @override
  _FilterState createState() => _FilterState();
}

class _FilterState extends State<Filter> {

  final TextEditingController _controllerSearchCarBrand = TextEditingController();
  final TextEditingController _controllerSearchWorkType = TextEditingController();

  RegExp workTypePattern = RegExp(r'');
  RegExp carBrandPattern = RegExp(r'');

  List<Result> workTypes = [];
  List<Result> carBrands = [];

  Result? selectWorkType;
  Result? selectCarBrand;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ThemeApp.blue,
      ),
      body: BlocBuilder<FilterPageBloc, FilterPageState>(
        builder: (context, page){
          if(page is FilterPageCarBrand){
            return BlocBuilder<CarBrandsBloc, CarBrandsState>(
              builder: (context, stateCarBrand){
                if(stateCarBrand is WorkTypeLoadingState){

                } else if(stateCarBrand is CarBrandsUnAuthState){

                } else if(stateCarBrand is CarBrandsFailedState){

                } else if(stateCarBrand is CarBrandsNetworkErrorState){

                } else if(stateCarBrand is CarBrandsDoneState){
                  carBrands = stateCarBrand.carBrands.result
                      .where((Result e) => (carBrandPattern.hasMatch(e.name)),
                  ).toList();
                  return SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.all(10.0),
                          child: TextFormField(
                            style: const TextStyle(fontSize: 16),
                            controller: _controllerSearchCarBrand,
                            onChanged: (pattern) {
                              setState(() {
                                carBrandPattern =
                                    RegExp(pattern, caseSensitive: false);
                              });
                            },
                            decoration: InputDecoration(
                                counterText: '',
                                counterStyle: const TextStyle(fontSize: 0),
                                suffixIcon: Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: _controllerSearchCarBrand.text.isEmpty
                                      ? const Icon(Icons.search)
                                      : IconButton(
                                    onPressed: () {
                                      setState(() {
                                        _controllerSearchCarBrand.text = '';
                                        carBrandPattern = RegExp('');
                                      });
                                    },
                                    icon: const Icon(Icons.close),
                                  ),
                                ),
                                suffixIconConstraints: const BoxConstraints(
                                    minWidth: 0, minHeight: 0),
                                border: OutlineInputBorder(
                                    borderSide:
                                    BorderSide(color: ThemeApp.blue)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide:
                                    BorderSide(color: ThemeApp.blue)),
                                hintText: Language.enterCarBrand),
                          ),
                        ),
                        ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: carBrands.length,
                          itemBuilder: (context, index){
                            return Card(
                              child: ListTile(
                                onTap: (){
                                  selectCarBrand = carBrands[index];
                                  context.read<FilterPageBloc>().add(ChangeFilterPage(page: FilterPage.workType));
                                },
                                title: Text(carBrands[index].name),
                                trailing: Icon(Icons.arrow_forward_ios_rounded),
                              ),
                            );
                          }
                        ),
                      ],
                    ),
                  );
                }
                return SizedBox.shrink();
              },
            );
          } else if(page is FilterPageWorkType){
            return BlocBuilder<WorkTypeBloc, WorkTypeState>(
              builder: (context, stateWorkType){
                if(stateWorkType is WorkTypeLoadingState){

                } else if(stateWorkType is WorkTypeUnAuthState){

                } else if(stateWorkType is WorkTypeFailedState){

                } else if(stateWorkType is WorkTypeNetworkErrorState){

                } else if(stateWorkType is WorkTypeDoneState){
                  workTypes = stateWorkType.workTypes.result
                      .where((Result e) => (workTypePattern.hasMatch(e.name)),
                  ).toList();
                  return SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.all(10.0),
                          child: TextFormField(
                            style: const TextStyle(fontSize: 16),
                            controller: _controllerSearchWorkType,
                            onChanged: (pattern) {
                              setState(() {
                                workTypePattern =
                                    RegExp(pattern, caseSensitive: false);
                              });
                            },
                            decoration: InputDecoration(
                                counterText: '',
                                counterStyle: const TextStyle(fontSize: 0),
                                suffixIcon: Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: _controllerSearchCarBrand.text.isEmpty
                                      ? const Icon(Icons.search)
                                      : IconButton(
                                    onPressed: () {
                                      setState(() {
                                        _controllerSearchWorkType.text = '';
                                        workTypePattern = RegExp('');
                                      });
                                    },
                                    icon: const Icon(Icons.close),
                                  ),
                                ),
                                suffixIconConstraints: const BoxConstraints(
                                    minWidth: 0, minHeight: 0),
                                border: OutlineInputBorder(
                                    borderSide:
                                    BorderSide(color: ThemeApp.blue)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide:
                                    BorderSide(color: ThemeApp.blue)),
                                hintText: Language.workTypeName),
                          ),
                        ),
                        ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: workTypes.length,
                            itemBuilder: (context, index){
                              return Card(
                                child: ListTile(
                                  onTap: (){
                                    selectWorkType = workTypes[index];
                                    Navigator.of(context).pop({
                                      'workType' : selectWorkType?.id,
                                      'carBrand' : selectCarBrand?.id,
                                    });
                                  },
                                  title: Text(workTypes[index].name),
                                  trailing: Icon(Icons.arrow_forward_ios_rounded),
                                ),
                              );
                            }
                        ),
                      ],
                    ),
                  );
                }
                return SizedBox.shrink();
              },
            );
          }
          return SizedBox.shrink();
        },
      ),
    );
  }
}
