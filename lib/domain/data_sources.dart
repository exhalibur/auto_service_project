import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:connectivity_plus/connectivity_plus.dart';

import '../core/dictionaries/constants.dart';
import '../core/error/exceptions.dart';
import '../core/error/failure.dart';
import '../core/singletons/local_storage.dart';
import '../data/data_sources.dart';
import 'package:dartz/dartz.dart';

/// Enum методов Rest запросов
enum Methods{get, post, put, delete}

/// Объявление коннектора к Запросам
typedef Constructor<T> = T Function(Map<String, dynamic>);

/// Класс отправки запроса данных
class SendRequest<T extends Type0>{
  http.Client client = http.Client();

  /// Обработки ошибок при запросе
  Future<Either<Failure, T>> getResponse(Params params) async {
    try{
      final remoteSendMessage = await sendRequest(params);
      return Right(remoteSendMessage);
    }on ServerException catch (e){
      return Left(ServerFailure(message: e.message));
    }on UnAuthException catch (e){
      return Left(UnAuthFailure(message: e.message));
    }on ConnectException catch (e){
      return Left(ConnectionFailure(message: e.message));
    } catch(_){
      return Left(ServerFailure(message: 'Ошибка сервера'));
    }
  }

  /// Отправка запроса
  Future<T> sendRequest(Params params) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      try{
        late http.Response re;
        Map<String, String> headers = {
          'Authorization' : LocalStorage.getString(AppConstants.TOKEN),
          'Content-Type': "application/json"
        };
        if(params.method == Methods.get){
          re = await client.get(params.url,
            headers: headers,
          );
        } else if(params.method == Methods.post){
          re = await client.post(params.url,
              headers: headers,
              body: jsonEncode(params.body)
          );
        } else if(params.method == Methods.put){
          re = await client.put(params.url,
              headers: headers,
              body: jsonEncode(params.body)
          );
        } else if(params.method == Methods.delete){
          re = await client.delete(params.url,
              headers: headers,
              body: jsonEncode(params.body)
          );
        }

        final T body = Type0.fromJson<T>(jsonDecode(utf8.decode(re.bodyBytes)));
        print('status: ${re.statusCode}, body: ${re.body}');
        if(re.statusCode == 200){
          return body;
        }else if(re.statusCode == 401){
          throw UnAuthException();
        }else{
          throw ServerException(message: "Ошибка сервера");
        }
      } on SocketException catch(_){
        throw ConnectException(message: 'Нет соединения с Сервером');
      } catch(_){
        rethrow;
      }
      finally{
        client.close();
      }
    } else {
      throw ConnectException(message: 'Нет соединения к интернету');
    }
  }
}

/// Абстрактный класс параметров запроса
class Params{
  final Methods method;
  final Uri url;
  Map<String, dynamic> body;
  Params({
    required this.method,
    required this.url,
    this.body = const {},
  });
}