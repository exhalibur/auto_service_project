import 'package:flutter/services.dart';

class MobileFormater extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue,
      TextEditingValue newValue  ) {
    final int textLength = newValue.text.length;
    int selectionIndex = newValue.selection.end;
    int usedSubstringIndex = 0;
    final StringBuffer text = StringBuffer();
    if (textLength >= 1) {
      text.write('(');
      if (newValue.selection.end >= 1) selectionIndex++;
    }
    if (textLength >= 5) {
      text.write(newValue.text.substring(0, usedSubstringIndex = 4) + ')');
      if (newValue.selection.end >= 4) {
        selectionIndex ++;
      }
    }
    if (textLength >= 7) {
      text.write(newValue.text.substring(4, usedSubstringIndex = 6) + '-');
      if (newValue.selection.end >= 6) {
        selectionIndex ++;
      }
    }
    // if (textLength >= 9) {
    //   text.write(newValue.text.substring(6, usedSubstringIndex = 8) + '-');
    //   if (newValue.selection.end >= 8) {
    //     selectionIndex ++;
    //   }
    // }
    if (textLength >= usedSubstringIndex) {
      text.write(newValue.text.substring(usedSubstringIndex));
    }
    return TextEditingValue(
      text: text.toString(),
      selection: TextSelection.collapsed(offset: selectionIndex),
    );
  }
}