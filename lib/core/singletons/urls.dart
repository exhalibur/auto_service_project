/// Модуль url's в приложении

enum Servers{test, prod}

class Urls{

  static Urls get _singletonTest => Urls._test();
  static Urls get _singletonProd => Urls._prod();

  static Urls _urlsInstance = _singletonTest;

  factory Urls.init(Servers server){
    if(server == Servers.test){
      /// Тестовый сервер :
      _urlsInstance = _singletonTest;
    }else if(server == Servers.prod){
      _urlsInstance = _singletonProd;
    }
    return _urlsInstance;
  }

  late String
    _srv, /// Основной адрес сервера
    _prefix, /// EndPoint прокси сервера
    _keyApplicationId, /// keyApplicationId для Parse
    _keyParseServerUrl, /// keyParseServerUrl для Parse
    _keyParseLiveQueryUrl, /// liveQueryUrl для Parse
    _keyClientKey /// clientKey для Parse
  ;

  static ParseConnectivityData get parse => ParseConnectivityData(
    keyApplicationId: Urls._urlsInstance._keyApplicationId,
    keyParseServerUrl: Urls._urlsInstance._keyParseServerUrl,
    keyParseLiveQueryUrl: Urls._urlsInstance._keyParseLiveQueryUrl,
    keyClientKey: Urls._urlsInstance._keyClientKey,
  );

  /// Текущие Urls
  static Uri get login => getUri('/login');
  static Uri get registration => getUri('/registration');
  static Uri get workTypes => getUri('/info/work_types');
  static Uri get carBrands => getUri('/info/car_brands');
  static Uri get services => getUri('/car_services/filter');
  static Uri get logout => getUri('/logout');

  Urls._test(){
    _srv = 'fix-moe-auto.store';
    _prefix = '/api';

  }

  Urls._prod(){
    _srv = '127.0.0.1';
    _prefix = '';
  }

  static Uri getUri(String path){
    return Uri.https(Urls._urlsInstance._srv, '${Urls._urlsInstance._prefix}$path');
  }
}

class ParseConnectivityData{
  final String keyApplicationId;
  final String keyParseServerUrl;
  final String keyParseLiveQueryUrl;
  final String keyClientKey;
  ParseConnectivityData({
    required this.keyApplicationId,
    required this.keyParseServerUrl,
    required this.keyParseLiveQueryUrl,
    required this.keyClientKey,
  });
}