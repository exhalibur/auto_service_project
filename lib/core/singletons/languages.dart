
enum Languages{rus, eng, lat}

class Language{
  static Language get _singletonRus => Language._rus();
  static Language get _singletonEng => Language._eng();
  static Language get _singletonLat => Language._lat();
  static Language _langInstance = _singletonRus;
  factory Language.init(Languages lang){
    if(lang == Languages.rus){
      _langInstance = _singletonRus;
    }else if(lang == Languages.eng){
      _langInstance = _singletonEng;
    }else if(lang == Languages.lat){
      _langInstance = _singletonLat;
    }
    return _langInstance;
  }

  static void changeState(Languages lang) => Language.init(lang);

  static late String
    /// Авторизация
    authorization,
    welcome,
    phone,
    password,
    forgotPassword,
    failure,
    enter,
    noAccount,
    register,
    /// Регистрация
    registration,
    gladToMeet,
    confirmPassword,
    registrationUser,
    /// Восстановление пароля
    passwordRecovery,
    restorePassword,
    /// Фильтр
    enterCarBrand,
    workTypeName,
    findServices,
    save,
    route,
    carBrand,
    workTypes,

    language
  ;

  Language._rus(){
    authorization = 'Авторизация';
    welcome = 'Добро пожаловать!';
    phone = 'Телефон';
    password = 'Пароль';
    forgotPassword = 'Забыли пароль?';
    failure = 'Ошибка';
    enter = 'Войти';
    noAccount = 'Нет аккаунта?';
    register = 'Зарегистируйтесь';

    registration = 'Регистрация';
    gladToMeet = 'Рады познакомиться!';
    confirmPassword = 'Подтвердите пароль';
    registrationUser = 'Зарегистрироваться';

    passwordRecovery = 'Восстановление пароля';
    restorePassword = 'Восстановить пароль';

    enterCarBrand = 'Введите марку машины';
    workTypeName = 'Название услуги';
    findServices = 'Найти сервисы';
    save = 'Сохранить';
    route = 'Маршрут';
    carBrand = 'Марки машин';
    workTypes = 'Типы работ';

    language = 'Язык';
  }

  Language._eng(){
    authorization = 'Authorization';
    welcome = 'Welcome!';
    phone = 'Phone';
    password = 'Password';
    forgotPassword = 'Forgot your password?';
    failure = 'Error';
    enter = 'Enter';
    noAccount = 'No account?';
    register = 'Register';

    registration = 'Registration';
    gladToMeet = 'Nice to meet you!';
    confirmPassword = 'Confirm password';
    registrationUser = 'Register';

    passwordRecovery = 'Password Recovery';
    restorePassword = 'Restore password';

    enterCarBrand = 'Enter car brand';
    workTypeName = 'Service name';
    findServices = 'Find Services';
    save = 'Save';
    route = 'Route';
    carBrand = 'Car brands';
    workTypes = 'Work types';

    language = 'Language';
  }

  Language._lat(){
    authorization = 'Autorizācija';
    welcome = 'Laipni lūgti!';
    phone = 'Tālrunis';
    password = 'Parole';
    forgotPassword = 'Aizmirsāt paroli?';
    failure = 'Kļūda';
    enter = 'Ievadiet';
    noAccount = 'Nav konta?';
    register = 'Reģistrēties';

    registration = 'Reģistrācija';
    gladToMeet = 'Prieks iepazīties!';
    confirmPassword = 'Apstiprināt paroli';
    registrationUser = 'Reģistrēties';

    passwordRecovery = 'Paroles atkopšana';
    restorePassword = 'Atjaunot paroli';

    enterCarBrand = 'Ievadiet automašīnas zīmolu';
    workTypeName = 'Pakalpojuma nosaukums';
    findServices = 'Atrast pakalpojumus';
    save = 'Saglabāt';
    route = 'Maršruts';
    carBrand = 'Automašīnu zīmoli';
    workTypes = 'Darba veidi';

    language = 'Valoda';
  }
}