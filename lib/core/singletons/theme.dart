
import 'package:flutter/material.dart';

enum Themes{light, dark}

class ThemeApp{
  static ThemeApp get _singletonLight => ThemeApp._light();
  static ThemeApp get _singletonDark => ThemeApp._dark();
  static ThemeApp _themeInstance = _singletonLight;
  factory ThemeApp.init(Themes theme){
    if(theme.name == Themes.light.name){
      _themeInstance = _singletonLight;
    }else if(theme.name == Themes.dark.name){
      _themeInstance = _singletonDark;
    }
    return _themeInstance;
  }

  final Color _blue = Colors.lightBlueAccent,
    _lightBlue = const Color(0xffe4f0f0),
    _darkBlue = Colors.lightBlueAccent,
    _backgroundBlue = const Color(0xfff0f7f7),
    _black = const Color(0xff333333),
    _grey = const Color(0xffD9D9D9),
    _lightGrey = const Color(0xffF1F1F1),
    _white = const Color(0xffFFFFFF),
    _darkRed = const Color(0xffef373e),
    _green = const Color(0xff3D9943);

  late String _backgroundImage;
  late Brightness _brightness;
  late Color _textColorMain;

  static Color get green => ThemeApp._themeInstance._green;
  static Color get darkRed => ThemeApp._themeInstance._darkRed;
  static Color get blue => ThemeApp._themeInstance._blue;
  static Color get lightBlue => ThemeApp._themeInstance._lightBlue;
  static Color get darkBlue => ThemeApp._themeInstance._darkBlue;
  static Color get backgroundBlue => ThemeApp._themeInstance._backgroundBlue;
  static Color get black => ThemeApp._themeInstance._black;
  static Color get grey => ThemeApp._themeInstance._grey;
  static Color get lightGrey => ThemeApp._themeInstance._lightGrey;
  static Color get white => ThemeApp._themeInstance._white;
  static Color get textColorMain => ThemeApp._themeInstance._textColorMain;
  static Brightness get brightness => ThemeApp._themeInstance._brightness;
  static bool get isDark => ThemeApp._themeInstance._brightness == Brightness.dark;
  static String get backgroundImage => ThemeApp._themeInstance._backgroundImage;
  static LinearGradient get linearGradientButton => LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [
      ThemeApp.isDark ? ThemeApp.blue.withOpacity(0.3) : ThemeApp.blue.withOpacity(0.8),
      ThemeApp.isDark ? ThemeApp.black : ThemeApp.blue.withOpacity(0.1),
    ],
  );

  ThemeApp._light(){
    _brightness = Brightness.light;
    _textColorMain = const Color(0xff0c2324);
    _backgroundImage = '';
  }
  ThemeApp._dark(){
    _brightness = Brightness.dark;
    _textColorMain = const Color(0xffFFFFFF);
    _backgroundImage = '';
  }

  static void changeTheme() =>
    ThemeApp.isDark ? ThemeApp.init(Themes.light) : ThemeApp.init(Themes.dark);
}