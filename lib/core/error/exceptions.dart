/// Модуль описания специальных Exception's в программе

class ServerException implements Exception {
  final String message;
  ServerException({required this.message});
}
class UnAuthException implements Exception {
  String message;
  UnAuthException({this.message = ''});
}
class CacheException implements Exception {
  final String message;
  CacheException({required this.message});
}
class ConnectException implements Exception {
  final String message;
  ConnectException({required this.message});
}
class FormatException implements Exception {
  final String message;
  FormatException({required this.message});
}