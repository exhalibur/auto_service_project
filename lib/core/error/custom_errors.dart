/// Модуль ошибок мобильного приложения

class Errors{
  static const String connectionFailed = 'Отсутствует соединение к интернету';
  static const String emptyString = 'Поле не может быть пустым';
  static const String userFailed = 'Данные о пользователе небыли сохранены';
  static const String failedType = 'Неверный формат данных';
  static const String unAuthorized = 'unAuth';
  static const String undefinedPerson = 'Пользователь с таким номером не найден';
  static const String paymentsError = 'Камера добавлена, однако у вас недостаточно средств для добавления подписок';
  static const String notEnoughPermissions = 'У вас недостаточно прав на доступ';
  static const String unValidFormatPhone = 'Формат телефона +371 9999 99 99';
  static const String paymentsErrorWhenAddCamera = 'Камера была добавлена, однако, для добавления подписок нехватает средств, для добавления необходимых подписок пополните баланс и измените подписки в настройка камеры';
  static const String shortPassword = 'Пароль должен состоять не менее чем из 8-ми символов';
  static const String unMatchPassword = 'Пароли не совпадают';
  static const String criticalServerErrorTitle = 'Произошла\nошибка сервера!';
  static const String criticalServerErrorDescription = 'На сервере произошла непредвиденная ошибка. Пожалуйста, подождите; она вскоре будет исправлена.';
}