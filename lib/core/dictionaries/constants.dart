/// Модуль списка констант программы
///
class AppConstants {
  static const String TOKEN = 'TOKEN';
  static const String THEME = 'THEME';
  static const String LANGUAGE = 'LANGUAGE';

  /// Справочники
  static const String WORK_TYPES = 'WORK_TYPES';
  static const String CAR_BRANDS = 'CAR_BRANDS';
}