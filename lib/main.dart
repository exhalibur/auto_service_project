import 'package:flutter/material.dart';
import 'package:flutter_blank/core/singletons/languages.dart';
import 'package:flutter_blank/core/singletons/local_storage.dart';
import 'package:flutter_blank/core/singletons/urls.dart';
import 'package:flutter_blank/data/data_sources.dart';
import 'package:flutter_blank/presentation/bloc/language_bloc/language_bloc.dart';
import 'package:flutter_blank/presentation/pages/main/main_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'core/dictionaries/constants.dart';
import 'core/dictionaries/routes.dart';
import 'presentation/pages/authorization/login.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await LocalStorage.init(); /// Инициализируем Локальное хранилище
  Urls.init(Servers.test); /// Инициализируем Server Routes
  Language.init(Languages.lat); /// Инициализируем Язык
  runApp(MultiBlocProvider(providers: [
    BlocProvider(
      create: (context) => LanguageBloc(),
    ),
  ], child: MyApp()));
}

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    final stateLang = context.watch<LanguageBloc>().state;
    if(stateLang is LanguageRus){
      Language.init(Languages.rus);
    }else if(stateLang is LanguageEng){
      Language.init(Languages.eng);
    }else if(stateLang is LanguageLat){
      Language.init(Languages.lat);
    }

    return MaterialApp(
      initialRoute: LocalStorage.getString(AppConstants.TOKEN).isNotEmpty ?
        AppRoutes.home : AppRoutes.login,
      routes: {
        AppRoutes.home: (context) => MainPage(),
        AppRoutes.login : (context) => Login(),
      },
    );
  }
}